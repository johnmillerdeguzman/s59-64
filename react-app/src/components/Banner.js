import Carousel from "react-bootstrap/Carousel";
import image1 from "../imgs/image1.png";
import image2 from "../imgs/image2.png";

export default function Banner() {
  return (
    <Carousel className="heightscreen col-12 mx-auto mt-5">
      <Carousel.Item>
        <img className="d-block imgCarousel img-fluid " src={image1} alt="JND" />

      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block imgCarousel img-fluid" src={image2} alt="JND" />
 
        
      </Carousel.Item>
    </Carousel>
  );
}


