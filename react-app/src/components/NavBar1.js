import { Button, Container, Form, Nav, Navbar } from "react-bootstrap";
import logo from "../imgs/logo.png";
import { Fragment, useContext } from "react";
import UserContext from "../UserContext";
import { NavLink } from "react-router-dom";
import { MDBIcon } from 'mdb-react-ui-kit';
import "../App.css";


import NavDropdown from 'react-bootstrap/NavDropdown';


export default function Navbar1() {
  const { user } = useContext(UserContext);

  return (
    <Navbar className="bg-nav-1 fixed-top" expand="md">
      <Container fluid>
        <Navbar.Brand as={NavLink} to="/">
          <img src={logo} className="logo1" alt="Logo" />
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="ms-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
            id="basic-nav-dropdown"
          >
          
    
            {user.id !== null ? (
              
              <NavDropdown title="User">
              
              <NavDropdown.Item as={NavLink} to="/signout">Sign Out</NavDropdown.Item>
            </NavDropdown>
            
            ) : (
              <Fragment>
                <Nav.Link as={NavLink} to="/signin">
                  Sign In
                </Nav.Link>

                <Nav.Link as={NavLink} to="/signUp">
                
                  Sign Up
                </Nav.Link>
                
              </Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
