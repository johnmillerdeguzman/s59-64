import "../App.css";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";
import {Col} from "react-bootstrap";
import React from 'react';

import {
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
} from "mdb-react-ui-kit";

function ProductsCard({ prop }) {
  const { _id, name, productType, price, image, stock, } = prop;

  return (

    <MDBContainer fluid className="my-2 section">
    <MDBRow>
    <Col>
      <MDBCol md="12" lg="12" className=" mb-lg-0">
        <MDBCard className="cards">
          <MDBCardImage
            src={image}
            position="top"
            alt={name}
          />
          <MDBCardBody>
            <div className="d-flex justify-content-between">
              <h5 className="mb-0 bodycard">{name}</h5>
              
            </div>
            <div><p className="bodybrand">{productType}</p></div>
            <div>
            <h5 className="text-dark mb-2 pricecard">₱ {price}</h5>
            </div>

            <div class="d-flex justify-content-between mb-2">
              <p class="text-muted pavail">
                Available: <span class="fw-bold">{stock}</span>
              </p>
              <div class="ms-auto text-warning">
              <Button as={Link} to={`/product/${_id}`} className="buttonbynow" color="primary">Buy now</Button>
              </div>
            </div>
          </MDBCardBody>
        </MDBCard>
        
      </MDBCol>  
      </Col>    
    </MDBRow>
  </MDBContainer>
    
  );
}

export default ProductsCard;
