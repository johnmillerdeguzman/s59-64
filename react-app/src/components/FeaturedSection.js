import "../App.css";
import React from 'react';




export default function FeaturedSection() {
  return (
    <div  class="background3">
    <h1 class="mb-3 sectiontitle">Services Offered</h1>
    

    
    <div class="d-lg-flex justify-content-center">

      <div className="serCard">
        <div className="card-body">
          <img src="https://www.rutter-net.com/hubfs/_rutter_2018/homepage/infrastructure-solutions.jpg" className="card-img-top"/>
          <h5 className="section-card-title mt-3">Network Infrastructure</h5>
          <p className="section-card-text">We offer network design, installation, and optimization services, along with robust security measures. Our team provides scalable solutions, monitoring, troubleshooting, and 24/7 technical support for uninterrupted connectivity and efficient data transfer.</p>
        </div>
      </div>
      
      <div className="serCard">
        <div className="card-body">
          <img src="https://jm1321.github.io/capstone1-deguzman/images/midwaymock1.png" className="card-img-top"/>
          <h5 className="section-card-title mt-3">Web Developement</h5>
          <p className="section-card-text">
          We provide comprehensive web development services, including custom website design, responsive development, e-commerce solutions, content management systems, and integration of advanced features for an engaging user experience and business growth.</p>
        </div>
      </div>
      <div className="serCard">
        <div className="card-body">
          <img src="https://static.wixstatic.com/media/395486_7e397f6e94bf464586580d83c22adeca~mv2_d_5472_3648_s_4_2.jpg/v1/crop/x_1021,y_0,w_3648,h_3648/fill/w_354,h_356,al_c,q_80,usm_0.66_1.00_0.01,enc_auto/DJI_0768_JPG.jpg" className="card-img-top"/>
          <h5 className="section-card-title mt-3">Drone Photography</h5>
          <p className="section-card-text">Our drone photography services offer stunning aerial imagery for various purposes. We capture high-resolution photos and videos from unique perspectives, delivering exceptional visual content for real estate, events, marketing, and more.</p>
        </div>
      </div>
    
      
      
    </div>
    
    
  </div>
  );
}


