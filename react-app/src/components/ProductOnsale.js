import "../App.css";
import { Link } from "react-router-dom";
import { Button, Card, Col, Row, ListGroup } from "react-bootstrap";

function ProductOnsale({ prop }) {
  const { _id, name, brand, price, image} = prop;

  return (
    <Row s={2} md={4} className="g-4">
    <Col className="col-2  offset-1 ">
      <Card border="secondary" className="mt-4 ">
        <Card.Body>
          <Card.Title className="text-dark">{name}</Card.Title>
        </Card.Body>
        <ListGroup variant="flush">
          <ListGroup.Item>Brand: {brand}</ListGroup.Item>
          <ListGroup.Item>Price: {price}</ListGroup.Item>
          <ListGroup.Item><img className="productImage" src={image}/></ListGroup.Item>
        </ListGroup>
        <Button as={Link} to={`/product/${_id}`} variant="primary">
          Details
        </Button>
      </Card>
    </Col>
    </Row>
  );
}

export default ProductOnsale;
