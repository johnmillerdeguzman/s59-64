import {Container, Row, Col} from 'react-bootstrap';

export default function Section() {
  return (
    <Container className='d-flex justify-content-lg-center fourSection'>
      <Row>
        <Col className='d-flex justify-content-lg-center'>
        <img src="https://cdn-icons-png.flaticon.com/512/3122/3122804.png"  width="50px" />
        <Row className='sectionDescription' >Free Shipping</Row>    
        </Col>
        <Col className='d-flex justify-content-lg-center'>
        <img src="https://cdn-icons-png.flaticon.com/512/3122/3122838.png"  width="50px" />
        <Row className='sectionDescription' >Guarantee Product</Row>    
        </Col>
        <Col className='d-flex justify-content-lg-center'>
        <img src="https://cdn-icons-png.flaticon.com/512/3122/3122794.png"  width="50px" />
        <Row className='sectionDescription' >Online Support</Row>    
        </Col>
        <Col className='d-flex justify-content-lg-center'>
        <img src="https://cdn-icons-png.flaticon.com/512/3122/3122789.png"  width="50px" />
        <Row className='sectionDescription' >Secure Payment</Row>    
        </Col>
      </Row>
    </Container>
  );
}

