import "../App.css";
import { Fragment } from "react";
import Card from "react-bootstrap/Card";
import { Row, Col } from "react-bootstrap";
import { useState, useEffect } from "react";
import {
  MDBRow,
  MDBCol,

} from 'mdb-react-ui-kit';


export default function ViewProducts() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/viewActive`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  return (
    <Fragment>
    <h2 className="sub-title">
    Products
      </h2>
    <div className="cardsHome d-flex justify-content-center">
      <Row xs={2} md={5} className="g-2 pdCards">
        {products.map((product) => {
          return (
            <Col>
              <Card className="cardHeight1">
                <MDBRow className="g-0">
                   <MDBCol md="12" className="mb-md-0 p-md-2">
                      <img
                          src={product.image}
                          className="productImage"
                          alt="..."/>
                          <h5 className="mt-0">{product.name}</h5>
                          <p>{product.brand}</p>
                          <p>₱ {product.price} </p>          
                          <a href={"./signin"} className="stretched-link"></a>      
                  </MDBCol>
                </MDBRow>
              </Card>
            </Col>
          );
        })}
      </Row>
      </div>
    </Fragment>
  );
}
