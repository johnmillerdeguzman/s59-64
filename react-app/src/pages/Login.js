import "../App.css";
import Form from "react-bootstrap/Form";
import {MDBContainer, MDBCol, MDBRow, MDBBtn, MDBIcon, MDBInput, MDBCheckbox } from 'mdb-react-ui-kit';
import { useState, useEffect, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import logo from "../imgs/logo.png";

export default function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(true);

  const { user, setUser } = useContext(UserContext);

 

  useEffect(() => {
    if (username !== "" && password !== "") {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [username, password]);

  function loginUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.accessToken !== "empty") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);

          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to our website!",
          });
        } else {
          Swal.fire({
            title: "Authentication failed!",
            icon: "error",
            text: "Check your login details and try again.",
          });
        }
      });

    const retrieveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => response.json())
        .then((data) => {
          console.log(data);

          setUser({ id: data._id, isAdmin: data.isAdmin });
        });
    };
  }

  if (user.isAdmin === true) {
    return <Navigate to="/adminDashboard" />;
  } else if (user.isAdmin === false && user.id !== null) {
    return <Navigate to="/productview" />;
  } else {
    return (
      <div className="pt-5">

      <MDBRow className="d-flex align-items-center justify-content-center">


        <MDBCol col='3' md='4'>

          <div className="pt-5 mt-5">
            <MDBRow className="d-flex align-items-center justify-content-center"><img src={logo}  alt="Sample image" style={{width:'200px'}}/></MDBRow>

          </div>
          <Form onSubmit={loginUser} className="p-3">
          <MDBInput wrapperClass='mb-4' 
                    label='Username'
                    id='formControlLg'
                    type='username'
                    size="lg"
                    value={username}
                    onChange={(event) => setUsername(event.target.value)}
                    required/>

          <MDBInput wrapperClass='mb-4'
                    label='Password'
                    id='formControlLg'
                    type='password'
                    size="lg"
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                    required
                    />
                   
         

          <div className='text-center text-md-start mt-4 pt-2 mb-13'>

            <MDBBtn className="mb-0 px-5 justify-content-center" size='lg' type="submit" disabled={isActive}>Login</MDBBtn>

            <p className="small fw-bold mt-2 pt-1 mb-2">Don't have an account? <a href="./signUp" className="link-danger">Register</a></p>
           
          </div>
          </Form>
        </MDBCol>

      </MDBRow>

      </div>
    );
  }
}
