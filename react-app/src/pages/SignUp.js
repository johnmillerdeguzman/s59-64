import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

import React from 'react';
import {
  MDBInput,
  MDBCol,
  MDBRow,
  MDBCheckbox,
  MDBBtn,
  MDBIcon
} from 'mdb-react-ui-kit';



import { Container, Row, Col } from "react-bootstrap";

//we use this to get the input of the user
import { useState, useEffect, useContext } from "react";
import logo from "../imgs/logo.png";
import Swal from "sweetalert2";

import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";

export default function Register() {
  //state hooks to store the values of the input field from our user
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);
  const { user, setUser } = useContext(UserContext);

 

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      password !== "" &&
      password2 !== "" &&
      password === password2 &&
      email !== "" &&
      username !== ""
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, password2, firstName, lastName, username]);

  //register features
  function registerUser(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/signup`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        username: username,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data.usernameExists) {
          Swal.fire({
            title: "Your username is already taken",
            icon: "error",
            text: "Please try another username.",
          });
        } else if (user) {
          Swal.fire({
            title: "Registration Complete",
            icon: "success",
            text: "Welcome to our website!",
          });
        }
        setUser({ id: data._id, isAdmin: data.isAdmin });
      });

    
  }

  return user.id !== null ? (
    <Navigate to="/signin" />
  ) : (
    <form onSubmit={registerUser} className="mt-5">
    <div className="pt-5 mt-5">
            <MDBRow className="d-flex align-items-center justify-content-center"><img src={logo}  alt="Sample image" style={{width:'200px'}}/></MDBRow>

          </div>
      <MDBRow className='mb-4 mtd-flex align-items-center justify-content-center' >
        <MDBCol className='mt-5' md='2'>
          <MDBInput 
          id='form3Example1' 
          label='First name'
          value={firstName}
          onChange={(event) => setFirstName(event.target.value)}
          required 
          />
        </MDBCol>
        <MDBCol className='mt-5' md='2'>
          <MDBInput
          id='form3Example2'
          label='Last name'
          value={lastName}
          onChange={(event) => setLastName(event.target.value)}
          required 
          
          />
        </MDBCol>
      </MDBRow>
      <MDBCol  md='2'>

        </MDBCol>
      <MDBRow className='d-flex align-items-center justify-content-center'>
      <MDBCol md='4'>
    
     

      <MDBInput
      className='mb-4' 
      type='email' 
      id='form3Example3' 
      label='Email address'
      value={email}
      onChange={(event) => setEmail(event.target.value)}
      required

      />

      <MDBInput 
      className='mb-4' 
      id='form3Example4' 
      label='Username'
      type="text"
      value={username}
      onChange={(event) => setUsername(event.target.value)}
      required

      />

      <MDBInput 
      className='mb-4'
      type='password' 
      id='form3Example4' 
      label='Password' 
      value={password}
      onChange={(event) => setPassword(event.target.value)}
      required

      />

      <MDBInput 
      className='mb-4' 
      type='password' 
      id='form3Example4'
      label='Confirm Password'
      value={password2}
      onChange={(event) => setPassword2(event.target.value)}
      required
      
      />
      </MDBCol>
      </MDBRow>
      <MDBRow className='d-flex align-items-center justify-content-center'>
      <MDBCol md='4'>
      <MDBBtn
      type='submit' 
      className='mb-4'
      block
      disabled={!isActive}
      >
        Sign in
      </MDBBtn>
      </MDBCol>
      </MDBRow>

      <div className='text-center'>
        <p>
          Already Sign Up? <a href='../signin'>Login</a>
        </p>
      </div>
    </form>
  );
}
