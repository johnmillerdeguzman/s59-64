import { Button, Card, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import ProductView from "./ProductView";


export default function SpecificProduct() {
  const cartPage = useNavigate();
  const { productId } = useParams();
  const [name, setName] = useState("");
  const [brand, setBrand] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [productType, setProductType] = useState("");
  const [image, setImage] = useState("");
  const [stock, setStock] = useState(0);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((response) => response.json())
      .then(
        (data) => {
          setName(data.name);
          setBrand(data.brand);
          setDescription(data.description);
          setPrice(data.price);
          setProductType(data.productType);
          setStock(data.stock);
          setImage(data.image);
        },
        [productId]
      );
    localStorage.setItem("productId", productId);
  });

  const readyToCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/addToCart`, {
      method: "POST",
      headers: {
        // "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);

        if (data !== null) {
          Swal.fire({
            title: "Do you want to order this product?",
            showDenyButton: true,
            confirmButtonText: "Proceed to checkout",
            denyButtonText: `Cancel`,
          }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
              Swal.fire("Please fill up the quantity", "", "success");
              cartPage(`/quantity/${data}`);
            } else if (result.isDenied) {
              Swal.fire("Order Cancelled", "", "info");
            }
          });
        }
      });
  };

  return (
    <div>
      <div class="flex-box">
        <div class="left">
            <div class="big-img">
                <img src={image}/>
            </div>
        </div>

        <div class="right">
            <div class="pname">{name}</div>
            <div class="bname">{brand}</div>
            <div class="dname">{description}</div>
            <div class="price">₱ {price}</div>
            <div class="quantity">
                <p>Quantity : {stock}</p>
            </div>
            <div class="btn-box">
            <Button class="cart-btn" onClick={() => readyToCart()}>
            Buy Now
            </Button>
            </div>
        </div>
      </div>
      <div>{ProductView}</div>
    </div>    
    
  );
}
