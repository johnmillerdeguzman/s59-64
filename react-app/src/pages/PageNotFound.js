import "../App.css";
import { Fragment } from "react";
import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <Fragment>
      <h1>Page Not Found!</h1>
      <div><img className="error"
      src={'https://img.freepik.com/free-vector/404-error-lost-space-concept-illustration_114360-7971.jpg?w=826&t=st=1689717868~exp=1689718468~hmac=761e1b4f8b46aabf7d2649349ef787c348f362b9a33c272a60908c9f493f614f'}/></div>
      <a className="removeline" href={"/"}><button class="button button5 notfoundbutton">Learn More</button></a>
    </Fragment>
  );
}
