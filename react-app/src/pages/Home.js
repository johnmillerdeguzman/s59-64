import { Fragment } from "react";
import Banner from "../components/Banner";
import FeaturedSection from "../components/FeaturedSection";
import Section from "../components/Section2";
import ViewProducts from "../components/ViewProducts"
import ViewProductsOnsale from "../components/ViewProductsOnsale";
import NewStuff from "../components/NewStuff";
import Footer from "../components/Footer";


export default function Home() {
  return (
    <Fragment>
      <Banner />
      <Section/>
      <FeaturedSection/>
      <NewStuff/>
      <ViewProductsOnsale />
      <ViewProducts />
      <Footer/>
    </Fragment>
  );
}
