import { Button, Col, Row, Container } from "react-bootstrap";
import { useState, useEffect, useContext } from "react";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";

export default function AddProduct() {
  const back = useNavigate();

  const [isActive, setIsActive] = useState(true);
  const [name, setName] = useState("");
  const [brand, setBrand] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [image, setImage] = useState("");
  const [productType, setProductType] = useState("");
  const [stock, setStock] = useState("");

  const { user } = useContext(UserContext);

  //useEffect for disable button in adding products
  useEffect(() => {
    if (
      name !== "" &&
      brand !== "" &&
      description !== "" &&
      price !== "" &&
      productType !== "" &&
      stock !== ""
    ) {
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [name, brand, description, price, productType, stock]);

  function addingProduct(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/createdNew`, {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        name: name,
        brand: brand,
        description: description,
        price: price,
        image:image,
        productType: productType,
        stock: stock,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.productExists) {
          Swal.fire({
            title: "New product added",
            icon: "success",
            text: "You successfully added the product.",
          });
          back("/adminDashboard");
        } else {
          Swal.fire({
            title: "Failed to add product",
            icon: "error",
            text: "Please check informations you entered.",
          });
          back("/adminDashboard");
        }
      });
  }

  return (
    <Container>
      <Row className="mt-5">
        <Col className="border-login">
          <Form onSubmit={addingProduct} className="p-3">
            <Form.Group className="mb-3" controlId="name">
              <h3 className="text-center">Add Product</h3>
              <Form.Label>
                <strong>Product Name:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Name"
                value={name}
                onChange={(event) => setName(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="brand">
              <Form.Label>
                <strong>Product Brand:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Brand"
                value={brand}
                onChange={(event) => setBrand(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="description">
              <Form.Label>
                <strong>Product Description:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Description"
                value={description}
                onChange={(event) => setDescription(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="price">
              <Form.Label>
                <strong>Product Price:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Price"
                value={price}
                onChange={(event) => setPrice(event.target.value)}
                required
              />
            </Form.Group>
            
            <Form.Group className="mb-3" controlId="image">
              <Form.Label>
                <strong>Image:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Image link"
                value={image}
                onChange={(event) => setImage(event.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="productType">
              <Form.Label>
                <strong>Product Product Type:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Product Type"
                value={productType}
                onChange={(event) => setProductType(event.target.value)}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="stock">
              <Form.Label>
                <strong>Product Stock:</strong>
              </Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product Stock"
                value={stock}
                onChange={(event) => setStock(event.target.value)}
                required
              />
            </Form.Group>

            <Button variant="primary" type="submit" disabled={isActive}>
              Add
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
