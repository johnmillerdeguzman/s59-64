import { Navigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import Swal from "sweetalert2";

export default function Unsetonsale() {
  const { productId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/unsetonsale/${productId}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.unsetonsaleDone) {
          Swal.fire({
            title: "Product Sale is Off",
            icon: "success",
            text: "You successfully Off the product.",
          });
        } else {
          Swal.fire({
            title: "Failed to set off product",
            icon: "error",
            text: "There's an error in setting off product.",
          });
        }
      });
  });
  return <Navigate to="/adminDashboard" />;
}
