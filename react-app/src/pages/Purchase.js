import { Button, Card, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";

export default function Purchase() {
  const [amount, setAmount] = useState("");
  const [date, setDate] = useState("");
  // const [orderId, setOrderId] = useState("");

  // setOrderId(localStorage.getItem("orderId"));

  const { orderId1 } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/checkout/${orderId1}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        setAmount(data.totalAmount);
        setDate(data.purchaseDate);
      });
  }, []);

  return (
    <Col className="col-8 offset-2 mt-5">
      <Card className="checkoutCard">
        <Card.Header>Successfully checkout!</Card.Header>
        <Card.Body>
          <Card.Title>Total Amount:</Card.Title>
          <Card.Text>{amount}</Card.Text>
          <Card.Title>Checkout Date:</Card.Title>
          <Card.Text>{date}</Card.Text>

          <Button as={Link} to={`/productview`} variant="primary">
            Buy more
          </Button>
        </Card.Body>
      </Card>
    </Col>
  );
}
