import "../App.css";
import ProductsCard from "../components/ProductsCard";
import Banner from "../components/Banner";
import { Fragment, useEffect, useState } from "react";




export default function ProductView() {
  const [products, setProducts] = useState([]);


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/viewActive`)
      .then((response) => response.json())
      .then((data) => {
        setProducts(
          data.map((product) => {
            return <ProductsCard key={product._id} prop={product} />;
          })
        );
      });
  });
  return (

    <Fragment>


      <div><Banner /></div>
      <h2 className="prodtitle">Products</h2>
      <div className="Onlinecards">{products}</div>
    </Fragment>
  );
}
