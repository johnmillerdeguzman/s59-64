import { Button, Col, Row, Container, Table } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext";
import { Navigate, Link } from "react-router-dom";
import { FaBeer } from 'react-icons/fa';
import { FcFlashOn, FcFlashOff } from 'react-icons/fc';
import { RxCross1 } from 'react-icons/rx';
import { BsCheck2All } from 'react-icons/bs';
import "../App.css";

function AdminDashboard() {
  // const { name, brand, description, price, productType, stock } = productProp;
  const { user } = useContext(UserContext);

  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/list`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
  }, []);

  return user.isAdmin !== true ? (
    <Navigate to="*" />
  ) : (
    <div className="p-4">
      <Row className="justify-content-center text-center">
        <h2>Admin Dashboard</h2>
        <div>
          <Button as={Link} to={`/addproduct`} variant="primary">
            Add New Product
          </Button>
          {/* <Button variant="success">Show User Orders</Button> */}
        </div>
        <Table striped bordered hover className="mt-2 ">
          <thead>
            <tr>
              <th>Name</th>
              <th>Brand</th>
              <th>Description</th>
              <th>Price</th>
              <th>Type</th>
              <th>Stock</th>
              <th>Active</th>
              <th>Onsale</th>
              <th>Controls</th>
            </tr>
          </thead>
          <tbody >
            {products.map((product) => {
              return (
                <tr className="adminDashboard" key={product._id}>
                  <td className="name">{product.name}</td>
                  <td className="brand">{product.brand}</td>
                  <td className="description">{product.description}</td>
                  <td className="tdprice">PHP {product.price}</td>
                  <td className="type">{product.productType}</td>
                  <td>{product.stock} stock(s)</td>
                  <td>{String(product.isActive)}</td>
                  <td>{String(product.onSale)}</td>
                  <td>
                    {product.isActive ? (
                      <Button
                        as={Link}
                        to={`/archive/${product._id}`}
                        variant="outline-transparent"
                      >
                      <BsCheck2All />
                      </Button>
                    ) : (
                      <Button
                        as={Link}
                        to={`/unarchive/${product._id}`}
                        variant="outline-transparent"
                      >
                      <RxCross1 />
                      </Button>
                    )},
                    {product.onSale ? (
                      <Button
                        as={Link}
                        to={`/unsetonsale/${product._id}`}
                        variant="outline-transparent"
                        
                      >
                      <FcFlashOn />
                      </Button>
                    ) : (
                      <Button
                        as={Link}
                        to={`/onsale/${product._id}`}
                        variant="outline-transparent"
                      >
                      <FcFlashOff />
                      </Button>
                    )}

                    <Button
                      as={Link}
                      to={`/updateProducts/${product._id}`}
                      variant="outline-primary"
                    >
                      Update
                    </Button>
                  </td>

                </tr>
              );
            })}
          </tbody>
        </Table>
      </Row>
    </div>
  );
}

export default AdminDashboard;
