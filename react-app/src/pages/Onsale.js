import { Navigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import Swal from "sweetalert2";

export default function Onsale() {
  const { productId } = useParams();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/onsale/${productId}`, {
      method: "PATCH",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        if (data.onsaleDone) {
          Swal.fire({
            title: "Product Onsale",
            icon: "success",
            text: "You successfully Onsale the product.",
          });
        } else {
          Swal.fire({
            title: "Failed to Onsale product",
            icon: "error",
            text: "There's an error in onsale product.",
          });
        }
      });
  });
  return <Navigate to="/adminDashboard" />;
}
